# Tutorial Sphinx

## Links úteis

* [reStructuredText](https://docutils.sourceforge.io/docs/user/rst/quickref.html)

* [Sphinx primeiros passos](https://www.sphinx-doc.org/pt_BR/1.6/tutorial.html)

* [Diversos temas para a página](https://sphinx-themes.org/)

* [Sphinx domains](https://www.sphinx-doc.org/en/master/usage/restructuredtext/domains.html#cross-referencing-python-objects)